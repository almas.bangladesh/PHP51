<?php
    $x = array(1, 2, "three", 4);
    $y = (object)array("key1"=>"value1", "key2"=>"value2");

    if(is_array($x)){
        echo "\$x is an array<br>";
    }
    else
    {
        echo "\$x is not an array<br>";
    }

    if(is_array($y)){
        echo "\$y is an array";
    }
    else
    {
        echo "\$y is not an array";
    }
?>