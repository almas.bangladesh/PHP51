<?php
    $x = 5;
    $y = 5;
    $z = '5';
    if($x == $y){
        echo 'Values of $x and $y are equal'."<br>";
    }
    if($x === $y){
        echo 'Values and types of $x and $y are also equal'."<br>";
    }
    else{
        echo '$x and $y are not equal'."<br>";
    }

    if($x != $z){
        echo 'Values of $x and $z are not equal'."<br>";
    }
    if($x !== $z){
        echo 'Values and types of $x and $z are not equal'."<br>";
    }
    else{
        echo '$x and $z are equal'."<br>";
    }

    if(5>5){
       echo "5 is greater than 5<br>";
    }
    else if(5<5){
        echo "5 is less than 5<br>";
    }
    else{
        echo "5 is equal to 5<br>";
    }

    if(5>=6){
        echo "5 is greater than or equal to 6<br>";
    }
    else if(5<=6){
        echo "5 is less than or equal to 6<br>";
    }
?>