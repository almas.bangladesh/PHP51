<?php
    class Calculator{
        private $data1;
        private $data2;
        public function setValues($data1=0, $data2=0){
            $this->data1 = $data1;
            $this->data2 = $data2;
        }
        public function addition(){
            return $this->data1+$this->data2;
        }
    }
    $additionCalc = new Calculator();
    if(empty($_GET['data1']) || empty($_GET['data2'])){
        echo "Input two values";
    }
    else{
        $additionCalc->setValues($_GET['data1'], $_GET['data2']);
        echo "Addition is ".$additionCalc->addition();
    }
?>